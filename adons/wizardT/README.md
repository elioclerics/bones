![](https://elioway.gitlab.io/ribs/wizardT/elio-boiler-t-logo.png)

> BoilerT, **the elioWay**

# wizardT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [wizardT Documentation](https://elioway.gitlab.io/ribs/wizardT/)

## Prerequisites

- [wizardT Prerequisites](https://elioway.gitlab.io/ribs/wizardT/installing.html)

## Installing

- [Installing wizardT](https://elioway.gitlab.io/ribs/wizardT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [wizardT Quickstart](https://elioway.gitlab.io/ribs/wizardT/quickstart.html)

# Credits

- [wizardT Credits](https://elioway.gitlab.io/ribs/wizardT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/wizardT/apple-touch-icon.png)
