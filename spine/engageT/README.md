![](https://elioway.gitlab.io/authT/engageT/elio-engage-t-logo.png)

> EngageT, **the elioWay**

# engageT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [engageT Documentation](https://elioway.gitlab.io/authT/engageT/)

## Prerequisites

- [engageT Prerequisites](https://elioway.gitlab.io/authT/engageT/installing.html)

## Installing

- [Installing engageT](https://elioway.gitlab.io/authT/engageT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [authT Quickstart](https://elioway.gitlab.io/authT/quickstart.html)
- [engageT Quickstart](https://elioway.gitlab.io/authT/engageT/quickstart.html)

# Credits

- [engageT Credits](https://elioway.gitlab.io/authT/engageT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/authT/engageT/apple-touch-icon.png)
