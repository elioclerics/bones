![](https://elioway.gitlab.io/ribs/destroyT/elio-destroy-t-logo.png)

> DeleteT, **the elioWay**

# destroyT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [destroyT Documentation](https://elioway.gitlab.io/ribs/destroyT/)

## Prerequisites

- [destroyT Prerequisites](https://elioway.gitlab.io/ribs/destroyT/installing.html)

## Installing

- [Installing destroyT](https://elioway.gitlab.io/ribs/destroyT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [destroyT Quickstart](https://elioway.gitlab.io/ribs/destroyT/quickstart.html)

# Credits

- [destroyT Credits](https://elioway.gitlab.io/ribs/destroyT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/destroyT/apple-touch-icon.png)
