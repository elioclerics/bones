![](https://elioway.gitlab.io/ribs/listT/elio-list-t-logo.png)

> ListT, **the elioWay**

# listT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [listT Documentation](https://elioway.gitlab.io/ribs/listT/)

## Prerequisites

- [listT Prerequisites](https://elioway.gitlab.io/ribs/listT/installing.html)

## Installing

- [Installing listT](https://elioway.gitlab.io/ribs/listT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [listT Quickstart](https://elioway.gitlab.io/ribs/listT/quickstart.html)

# Credits

- [listT Credits](https://elioway.gitlab.io/ribs/listT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/listT/apple-touch-icon.png)
