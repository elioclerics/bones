![](https://elioway.gitlab.io/ribs/readT/elio-read-t-logo.png)

> ReadT, **the elioWay**

# readT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [readT Documentation](https://elioway.gitlab.io/ribs/readT/)

## Prerequisites

- [readT Prerequisites](https://elioway.gitlab.io/ribs/readT/installing.html)

## Installing

- [Installing readT](https://elioway.gitlab.io/ribs/readT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [readT Quickstart](https://elioway.gitlab.io/ribs/readT/quickstart.html)

# Credits

- [readT Credits](https://elioway.gitlab.io/ribs/readT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/readT/apple-touch-icon.png)
