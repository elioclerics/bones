![](https://elioway.gitlab.io/ribs/unlistT/elio-unlist-t-logo.png)

> UnlistT, **the elioWay**

# unlistT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [unlistT Documentation](https://elioway.gitlab.io/ribs/unlistT/)

## Prerequisites

- [unlistT Prerequisites](https://elioway.gitlab.io/ribs/unlistT/installing.html)

## Installing

- [Installing unlistT](https://elioway.gitlab.io/ribs/unlistT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [unlistT Quickstart](https://elioway.gitlab.io/ribs/unlistT/quickstart.html)

# Credits

- [unlistT Credits](https://elioway.gitlab.io/ribs/unlistT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/unlistT/apple-touch-icon.png)
