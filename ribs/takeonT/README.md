![](https://elioway.gitlab.io/ribs/takeonT/elio-takeon-t-logo.png)

> TakeonT, **the elioWay**

# takeonT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [takeonT Documentation](https://elioway.gitlab.io/ribs/takeonT/)

## Prerequisites

- [takeonT Prerequisites](https://elioway.gitlab.io/ribs/takeonT/installing.html)

## Installing

- [Installing takeonT](https://elioway.gitlab.io/ribs/takeonT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [takeonT Quickstart](https://elioway.gitlab.io/ribs/takeonT/quickstart.html)

# Credits

- [takeonT Credits](https://elioway.gitlab.io/ribs/takeonT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/takeonT/apple-touch-icon.png)
