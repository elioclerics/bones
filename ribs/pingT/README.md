![](https://elioway.gitlab.io/ribs/pingT/elio-ping-t-logo.png)

> PingT, **the elioWay**

# pingT

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [pingT Documentation](https://elioway.gitlab.io/ribs/pingT/)

## Prerequisites

- [pingT Prerequisites](https://elioway.gitlab.io/ribs/pingT/installing.html)

## Installing

- [Installing pingT](https://elioway.gitlab.io/ribs/pingT/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [ribs Quickstart](https://elioway.gitlab.io/ribs/quickstart.html)
- [pingT Quickstart](https://elioway.gitlab.io/ribs/pingT/quickstart.html)

# Credits

- [pingT Credits](https://elioway.gitlab.io/ribs/pingT/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/ribs/pingT/apple-touch-icon.png)
